var RECORDMODULE, SEARCHMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search'], runUserEvent);

function runUserEvent(record, search) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	
	var returnObj = {};
//	returnObj['beforeLoad'] = _beforeLoad;
//	returnObj['beforeSubmit'] = _beforeSubmit;
	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');
	
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
	
//	logVar('scriptContext.type', scriptContext.type);
//	logRecord(scriptContext.newRecord, 'scriptContext.newRecord');
	
	log.debug({
		title: 'Record type - id',
		details: scriptContext.newRecord.type + ' - ' + scriptContext.newRecord.id
	});	
	log.debug({
		title: 'Context type',
		details: scriptContext.type
	});
	
	if (scriptContext.type == 'delete') {
		return;
	}
	
	/*
	 * fufillObj.soLinesToFulfill is an array of object {sopolink: , line: , quantityReceived: , quantityFulfilled: }
	 */
	// Only create item fulfillment when creating item receipt
	// Nothing to do when item receipt is being edited
	var fulfillObj;
	if (scriptContext.type == 'create') {
		fulfillObj = {
				soLinesToFulfill: []
		};
	}
	
	var itemReceiptRecord = scriptContext.newRecord
	var purchaseOrderRecord = fromIRtoPO(itemReceiptRecord, fulfillObj);
//	logRecord(result.purchaseOrderRecord, 'purchaseOrderRecord');
	var salesOrderRecord = fromPOtoSO(purchaseOrderRecord);
//	logRecord(salesOrderRecord, 'salesOrderRecord');
	fromSOtoIFs(salesOrderRecord, fulfillObj);
	
	// Only create item fulfillment when creating item receipt
	// Nothing to do when item receipt is being edited
	if (scriptContext.type == 'create') {
		var itemFulfillmentRecord = fulfillSO(salesOrderRecord, fulfillObj);
		if (itemFulfillmentRecord) {
			log.debug({
				title: 'Item fulfillment created',
				details: itemFulfillmentRecord.id
			});
		}		
	}
		
	log.debug('afterSubmit done');
}

function fromIRtoPO(itemReceiptRecord, fulfillObj) {
	log.debug('fromIRtoPO started');
	
	var lineCount = itemReceiptRecord.getLineCount({
		sublistId: 'item'
	});	
	if (!lineCount) { return; }
	
	var poId = itemReceiptRecord.getValue('orderid');	
	var purchaseOrderRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.PURCHASE_ORDER,
		id: poId,
		isDynamic: true
	});
	
//	logRecord(itemReceiptRecord, 'itemReceiptRecord');
	
	var isPOchanged = false;
	for (var i = 0 ; i < lineCount ; i++) {
		// Bypass lines that are not received
		var itemReceive = itemReceiptRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'itemreceive',
			line: i
		});
		if (!itemReceive) { continue; }
		
		// Bypass lines when orderline field is not populated (should not occur)
		var poLine = itemReceiptRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'orderline',
			line: i
		});
		if (!poLine) { continue; }
		
		// Look for the po line referenced by the item receipt line - skip if not found (should not occur)
		var poLineNumber = purchaseOrderRecord.findSublistLineWithValue({
			sublistId: 'item',
			fieldId: 'line',
			value: poLine
		});
		if (poLineNumber < 0) { continue; }
		
		var sopoLink = purchaseOrderRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_so_po_link',
			line: poLineNumber
		});
		if (sopoLink && fulfillObj) {
			var itemId = purchaseOrderRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'item',
				line: poLineNumber
			});
			
			var fieldsValues = SEARCHMODULE.lookupFields({
				type: SEARCHMODULE.Type.ITEM,
				id: itemId,
				columns: ['custitem_bwp_sp_drp']
			});
			// custitem_bwp_sp_drp field is a checkbox, the lookupfields result is a boolean
			if ('custitem_bwp_sp_drp' in fieldsValues &&  fieldsValues.custitem_bwp_sp_drp) {
				var quantity = purchaseOrderRecord.getSublistValue({
					sublistId: 'item',
					fieldId: 'quantity',
					line: poLineNumber
				});
				fulfillObj.soLinesToFulfill.push({sopolink: sopoLink, line: '', quantityReceived: quantity, quantityFulfilled: ''});
			}			
		}
		
		// Check if the serial no is different in the IR and in the PO - skip if not different
		purchaseOrderRecord.selectLine({
			sublistId: 'item',
			line: poLineNumber
		});
		var serialNo = itemReceiptRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			line: i
		});
		var poSerialNo = purchaseOrderRecord.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie'
		});
		if (serialNo == poSerialNo) { continue; }
		
		// Update the serial number on the purchase order line
		purchaseOrderRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			value: serialNo
		});
		purchaseOrderRecord.commitLine({
			sublistId: 'item'
		});
		
		isPOchanged = true;		
	}
	
	if (isPOchanged) {
		log.debug('Save updated PO');
		purchaseOrderRecord.save();
	}
	
	log.debug('fromIRtoPO done');
	return purchaseOrderRecord;
}

function fromPOtoSO(purchaseOrderRecord) {
	log.debug('fromPOtoSO started');
	
	var lineCount = purchaseOrderRecord.getLineCount({
		sublistId: 'item'
	});	
	if (!lineCount) { return; }
	
	var soId = purchaseOrderRecord.getValue('createdfrom');	
	if (!soId) { return; }
	
	var salesOrderRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.SALES_ORDER,
		id: soId,
		isDynamic: true
	});
	
	var isSOchanged = false;
	for (var i = 0 ; i < lineCount ; i++) {
		var sopoLink = purchaseOrderRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_so_po_link',
			line: i
		});
		if (!sopoLink) { continue; }
		
		// Look for the so line referenced by the purchase order line - skip if not found
		var soLineNumber = salesOrderRecord.findSublistLineWithValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_so_po_link',
			value: sopoLink
		});
		if (soLineNumber < 0) { continue; }
		
		// Check if the serial no is different in the PO and in the SO - skip if not different
		salesOrderRecord.selectLine({
			sublistId: 'item',
			line: soLineNumber
		});
		var serialNo = purchaseOrderRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			line: i
		});
		var soSerialNo = salesOrderRecord.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie'
		});
		if (serialNo == soSerialNo) { continue; }
		
		// Update the serial number on the sales order line
		salesOrderRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			value: serialNo
		});
		salesOrderRecord.commitLine({
			sublistId: 'item'
		});
		
		isSOchanged = true;		
	}
	
	if (isSOchanged) {
		log.debug('Save updated SO');
		salesOrderRecord.save();
	}

	log.debug('fromPOtoSO done');
	return salesOrderRecord;
}

function fromSOtoIFs(salesOrderRecord, fulfillObj) {
	log.debug('fromSOtoIFs started');
	
	var lineCount = salesOrderRecord.getLineCount({
		sublistId: 'item'
	});
	if (!lineCount) { return; }
	
	if (fulfillObj) {
		// find the line field value that corresponds to each sopolink value and populate the corresponding line value
		for (var i = 0 ; i < fulfillObj.soLinesToFulfill.length ; i++) {
			var soLineNumber = salesOrderRecord.findSublistLineWithValue({
				sublistId: 'item',
				fieldId: 'custcol_bwp_so_po_link',
				value: fulfillObj.soLinesToFulfill[i].sopolink
			});
			if (soLineNumber < 0) { continue; }
			
			var soLine = salesOrderRecord.getSublistValue({
				sublistId: 'item',
				fieldId: 'line',
				line: soLineNumber
			});
			fulfillObj.soLinesToFulfill[i].line = soLine;
		}		
	}
	
	var soId = salesOrderRecord.getValue('id');
	var searchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.ITEM_FULFILLMENT,
		columns: ['internalid'],
		filters: [ SEARCHMODULE.createFilter({
			name: 'createdFrom',
			operator: SEARCHMODULE.Operator.IS,
			values: soId
		})]
	});
	
	logRecord(fulfillObj, 'Before IF - soLinesReceived');
	
	searchObj.run().each(function(result) {
		var ifId = result.getValue('internalid');
//		fufillObj = fromSOtoIF(salesOrderRecord, soLinesToFulfill, ifId);
		fromSOtoIF(salesOrderRecord, fulfillObj, ifId);
	});
	
	logRecord(fulfillObj, 'After IF - soLinesReceived');

	log.debug('fromSOtoIFs done');	
}

function fromSOtoIF(salesOrderRecord, fulfillObj, ifId) {
	var itemFulfillmentRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.ITEM_FULFILLMENT,
		id: ifId,
		isDynamic: true
	});
	
	var lineCount = itemFulfillmentRecord.getLineCount({
		sublistId: 'item'
	});
	
	var isIFchanged = false;
	for (var i = 0 ; i < lineCount ; i++) {
		var soLine = itemFulfillmentRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'orderline',
			line: i
		});
		if (!soLine) { continue; }
		
		// Look for the so line referenced by the item fulfillment line - skip if not found
		var soLineNumber = salesOrderRecord.findSublistLineWithValue({
			sublistId: 'item',
			fieldId: 'line',
			value: soLine
		});
		if (soLineNumber < 0) { continue; }
		
		itemFulfillmentRecord.selectLine({
			sublistId: 'item',
			line: i
		});
		
		if (fulfillObj) {
			// Complete the soLinesToFulfill array with quantities already fulfilled 
			var index = fulfillObj.soLinesToFulfill.findIndex(function(element) { return element.line == soLine;} );
			if (index >= 0) {
				var ifQuantity = itemFulfillmentRecord.getCurrentSublistValue({
					sublistId: 'item',
					fieldId: 'quantity'
				});
				var quantityFulfilled = parseInt(fulfillObj.soLinesToFulfill[index].quantityFulfilled || 0, 10);
				fulfillObj.soLinesToFulfill[index].quantityFulfilled = quantityFulfilled + parseInt(ifQuantity, 10);
			}			
		}

		// Check if the serial no is different in the SO and in the IF - skip if not different
		var serialNo = itemFulfillmentRecord.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie'
		});
		var soSerialNo = salesOrderRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			line: soLineNumber
		});
		if (serialNo == soSerialNo) { continue; }
		
		// Update the serial number on the item fulfillment line
		itemFulfillmentRecord.setCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_numero_de_serie',
			value: soSerialNo
		});
		itemFulfillmentRecord.commitLine({
			sublistId: 'item'
		});
		
		isIFchanged = true;		
	}
	
	if (isIFchanged) {
		log.debug('Save updated IF');
		itemFulfillmentRecord.save();
		return itemFulfillmentRecord;
	}
}

function fulfillSO(salesOrderRecord, fufillObj) {
	log.debug('fulfillSO started');
	var isIfRequired = false;
	var itemFulfillmentRecord = {};
	for (var i = 0 ; i < fufillObj.soLinesToFulfill.length ; i++) {
		var quantityReceived = parseInt(fufillObj.soLinesToFulfill[i].quantityReceived || 0,10);
		var quantityFulfilled = parseInt(fufillObj.soLinesToFulfill[i].quantityFulfilled || 0,10);
		if (quantityReceived > 0 && quantityFulfilled == 0) {
			isIfRequired = true;
		}
	}
	
	if (!isIfRequired) {
		log.debug('No item fulfillment to create');
		return; 
	}
	
//	logRecord(salesOrderRecord, 'salesOrderRecord');
	itemFulfillmentRecord = RECORDMODULE.transform({
		fromType: RECORDMODULE.Type.SALES_ORDER,
		fromId: salesOrderRecord.id,
		toType: RECORDMODULE.Type.ITEM_FULFILLMENT,
		isDynamic: true
	});

//	logRecord(itemFulfillmentRecord, 'itemFulfillmentRecord');
	
	var lineCount = itemFulfillmentRecord.getLineCount({
		sublistId: 'item'
	});
	var lineToSelect = 0;
	for (var i = 0 ; i < lineCount ; i++) {
		log.debug({
			title: 'Processing line',
			details: i
		});
		/*
		 * Impossible do delete an item fulfillment line : returns a SSS_INVALID_SUBLIST_OPERATION error
		if (!isIfLineRequired) {
			log.debug('Remove if line');
			itemFulfillmentRecord.removeLine({
				sublistId: 'item',
				line: lineToSelect
			});
			continue;
		}
		*/		
		
		itemFulfillmentRecord.selectLine({
			sublistId: 'item',
			line: lineToSelect
		});
		
		// Calculate the quantity to fulfill	
		var orderLine = itemFulfillmentRecord.getCurrentSublistValue({
			sublistId: 'item',
			fieldId: 'orderline'
		});		
		var soLineToFulfill = fufillObj.soLinesToFulfill.find(function(element) { return element.line == orderLine;} )
		var quantityToFulfill = 0;
		if (soLineToFulfill) {
			var quantityReceived = parseInt(soLineToFulfill.quantityReceived || 0,10);
			var quantityFulfilled = parseInt(soLineToFulfill.quantityFulfilled || 0,10);
			if (quantityReceived > 0 && quantityFulfilled == 0) {
				// substract quantityFulfilled in prevision of a change
				quantityToFulfill = quantityReceived - quantityFulfilled;
			}			
		}
		
		if (!quantityToFulfill) {
			log.debug({
				title: 'IF line ' + lineToSelect + ' not fulfilled',
				details: ''
			});
			itemFulfillmentRecord.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'itemreceive',
				value: false
			});			
		} else {
			log.debug({
				title: 'IF line ' + lineToSelect + ' fulfilled',
				details: ''
			});
			itemFulfillmentRecord.setCurrentSublistValue({
				sublistId: 'item',
				fieldId: 'quantity',
				value: quantityToFulfill
			});			
		}
		
		itemFulfillmentRecord.commitLine({
			sublistId: 'item'
		});
		
		lineToSelect++
	}
	
	var id = itemFulfillmentRecord.save();

	log.debug('fulfillSO done');
	return itemFulfillmentRecord;
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}