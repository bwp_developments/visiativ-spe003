var RECORDMODULE, SEARCHMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search'], runUserEvent);

function runUserEvent(record, search) {
	RECORDMODULE= record;
	SEARCHMODULE= search;
	
	var returnObj = {};
//	returnObj['beforeLoad'] = _beforeLoad;
	returnObj['beforeSubmit'] = _beforeSubmit;
//	returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');
	
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	
	logVar('scriptContext.type', scriptContext.type);
	logRecord(scriptContext.newRecord, 'scriptContext.newRecord');
	
	var salesOrderRecord = scriptContext.newRecord;
	var sopolinkvalue = salesOrderRecord.getValue('custbody_bwp_so_po_linkvalue');
	if (!sopolinkvalue) {
		sopolinkvalue = 0;
	}
	
	var isSoModified = false;
	var lineCount = salesOrderRecord.getLineCount({
		sublistId: 'item'
	});
	for (var i = 0 ; i < lineCount ; i++) {
		var sopolink = salesOrderRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'custcol_bwp_so_po_link',
			line: i
		});
		if (! sopolink) {
			sopolinkvalue += 1;
			salesOrderRecord.setSublistValue({
				sublistId: 'item',
				fieldId: 'custcol_bwp_so_po_link',
				line: i,
				value: sopolinkvalue
			});
			isSoModified = true;
		}
	}
	
	if (isSoModified) {
		salesOrderRecord.setValue({
			fieldId: 'custbody_bwp_so_po_linkvalue',
			value: sopolinkvalue
		});
	}
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {
	log.debug('afterSubmit started');
		
	log.debug('afterSubmit done');
}

function logRecord(record, logTitle) {
	var logRecord = JSON.stringify(record);
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}